from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
import time

def delete0():
	driver = webdriver.Firefox()

	driver.get("http://192.168.1.179")
	time.sleep(3)

	driver.find_element_by_id("user").send_keys("admin")
	driver.find_element_by_id("pass").send_keys("admin")
	time.sleep(1)
	driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
	time.sleep(1)
	driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
	time.sleep(1)

	driver.find_element_by_id("drives-click").click()
	time.sleep(1)

	driver.find_element_by_id("raid-div").click()
	time.sleep(7)

	driver.find_element_by_link_text("Array1").click()
	time.sleep(2)

	driver.find_element_by_xpath("//span[text()='Delete Raid Array']").click()
	time.sleep(3)

	driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button//span[text()='Confirm']").click()
	time.sleep(2)

	text= driver.find_element_by_id("lvmvalidnum").text
	driver.find_element_by_id("lvmtext-add").send_keys(text)
	time.sleep(6)
	driver.find_element_by_id("id_of_button").click()
	time.sleep(15)
	driver.close()

delete0()