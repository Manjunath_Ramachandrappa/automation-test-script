from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
from Reporting import Report_Generation
import mkpkg

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)


element = sys.argv[1:]
i_str_for_split = str(element).split(',')
print(i_str_for_split[4])

# Reading input from commandline arguments which is being passed from calling function, for example, Smoke_Test_Run.py
i_Host_IP_Address = i_str_for_split[0].replace("['", "")
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = i_str_for_split[1]
logger.info("User Name" + i_user_name)
i_password = i_str_for_split[2]
logger.info("Password" + i_password)
i_logfile_name = i_str_for_split[3]
logger.info("Log File Name" + i_logfile_name)
i_HTML_Report_name = i_str_for_split[4]
logger.info("HTML Report File" + i_HTML_Report_name)

fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 209 Script Start")
logger.info("Sys argument Values ::" + str(sys.argv))
logger.info("")

#===============================================================================
# 
# driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")
# driver.get("http://192.168.1.239")'''
# 
# 
# #===============================================================================
# # 
# # '''chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
# # '''os.environ["webdriver.chrome.driver"] = chromedriver
# # '''driver = webdriver.Chrome(chromedriver)
#===============================================================================
#===============================================================================

#driver = mkpkg.getOrCreateWebdriver()
driver=webdriver.Firefox()
#driver.get("http://192.168.1.239")


def ENTC209():

    driver.get(i_Host_IP_Address)
    
    time.sleep(5)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    
    time.sleep(4)
    
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    
    time.sleep(3)
    
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    logger.info("Logged into Portal")
    time.sleep(5)
   
    driver.find_elements_by_xpath("//div[@id='drives-click']")[0].click()
    logger.info("successfully Clicked Drives")
    time.sleep(5)

    driver.find_element_by_xpath("//div[@id='drive-ref']").click()
    logger.info("Successfully Clicked inside Drives")
    time.sleep(8)


    
    driver.find_element_by_css_selector("input[type='radio'][id='driver_disk_array_Array1']").click()
    logger.info("Clciked successfully Array")
    time.sleep(5)

    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//div[@class='ui-dialog-content ui-widget-content']//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][2]").click()
    logger.info("Successfully clicked Format button")
    time.sleep(5)

    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='OK']").click()
    logger.info("successfully clciked ok button")
    time.sleep(7)

    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='confirmdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//div//button[1]").click()
    logger.info("successfully clciked another ok")
    time.sleep(5)

    text=driver.find_element_by_id("lvmvalidnum").text
    logger.info("captcha occurred")
    time.sleep(5)

    driver.find_element_by_id("lvmtext-add").send_keys(text)
    logger.info("We captured captcha successfully")
    time.sleep(5)

    driver.find_element_by_id("id_of_button").click()
    logger.info("Clicked ok button")
    time.sleep(120)

    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@id='alertdialog']//following::div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button").click()
    logger.info("Clicked Top anchor")
    time.sleep(5)

    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//div[@class='ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix ui-draggable-handle']//button//span[1]").click()
    logger.info("Clicked on ok")
    time.sleep(5)

    driver.find_element_by_xpath("//a[@id='drives-back']").click()
    logger.info("Backed to drivers")
    time.sleep(5)

    driver.find_element_by_xpath("//div[@id='filesharing-click']").click()
    logger.info("Clicked successfully in to filesharing")
    time.sleep(5)

    driver.find_element_by_xpath("//div[@id='folsetlan']").click()
    logger.info("Clicked in to share folders")
    time.sleep(5)

    if(driver.find_element_by_xpath("//div[@class='pq-grid-cont-inner pq-grid-norows']")):
        Report_Generation.AddRow(i_HTML_Report_name, 'Drives_ENTC_209', 'Array Format', 'Pass')

    else:
        Report_Generation.AddRow(i_HTML_Report_name, 'Drives_ENTC_209', 'Array Format', 'Fail')
    logger.info("Drives ENTC209 Test case is successfully executed")
    
try:    
    ENTC209()
except Exception as e:
    Report_Generation.AddRow(i_HTML_Report_name, 'Drives_ENTC_209', 'Array Format', 'Fail')
    logger.info("Exception ::    " + str(e))
    
logger.info("Drives ENTC 209 Script End")
driver.close()
        


