#coding: utf-8
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import os
import logging
import sys
#driver = webdriver.Ie("D:\\Office-Work\\Selenium_Framework\\IEDriverServer.exe")

logger = logging.getLogger("Smoke Test Logging")
logger.setLevel(logging.DEBUG)

i_Host_IP_Address = sys.argv[1]
logger.info("Host IP" + i_Host_IP_Address)
i_user_name = sys.argv[2]
logger.info("User Name" + i_user_name)
i_password = sys.argv[3]
logger.info("Password" + i_password)
i_logfile_name = sys.argv[4]
logger.info("Password" + i_logfile_name)


#fh = logging.FileHandler("F:\SMB_Logs.txt")
fh = logging.FileHandler(i_logfile_name)
fh.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
fh.setFormatter(formatter)
logger.addHandler(fh)

logger.info("================")
logger.info("Drives ENTC 209 Script Start")
logger.info("")


try:
    driver = webdriver.Firefox()
    driver.get(i_Host_IP_Address)

    '''chromedriver = "D:\Office-Work\chromedriver_win32\chromedriver.exe"
    os.environ["webdriver.chrome.driver"] = chromedriver
    driver = webdriver.Chrome(chromedriver)
    driver.get("http://192.168.1.179")'''

    time.sleep(1)
    
    driver.find_element_by_id("user").send_keys(i_user_name)
    driver.find_element_by_id("pass").send_keys(i_password)
    
    time.sleep(1)
    
    driver.find_elements_by_css_selector("input[type='radio'][value='1209600']")[0].click()
    
    time.sleep(1)
    
    driver.find_element_by_css_selector("input[type='submit'][value='OK']").click()
    
    logger.info("Logged into Portal")
    time.sleep(1)
    
    driver.find_elements_by_xpath("//div[@id='drives-click']")[0].click()
    
    time.sleep(1)
    logger.info("Drives Main Button Clicked successfully")
    
    driver.find_element_by_xpath("//div[@id='drive-ref']").click()
    logger.info("Drives Inner Page Button Clicked successfully")
    
    time.sleep(10)
    
    driver.find_element_by_id('driver_disk_array_DISK1').click()
    logger.info("Disk 1 clicked successfully")
    
    driver.find_element_by_xpath("//div[@class='pq-grid-toolbar pq-grid-toolbar-crud']//span['ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][2]").click()
    logger.info("Format Button Clicked successfully")
    time.sleep(1)
    
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'][1]").click()
    logger.info("Format Button OK successfully")
    time.sleep(1)
    
    #driver.find_elements_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only']//span[text()='Confirm']").click()
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[1]").click()
    logger.info("Format Button Second OK button Clicked successfully")
    time.sleep(10)
    
    text= driver.find_element_by_id("lvmvalidnum").text
    logger.info("Getting captch text")
    
    driver.find_element_by_id("lvmtext-add").send_keys(text)
    logger.info("Entering captch text into text box")
    time.sleep(6)
    
    driver.find_element_by_id("id_of_button").click()
    logger.info("Captcha OK button clicked successfully")
    time.sleep(100)
    
    logger.info("Format Completed")
    driver.find_element_by_xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-dialog-buttons ui-draggable ui-resizable']//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//span[text()='Ok']").click()
    logger.info("Confrimation complete ok button clicked sucessfully")
    time.sleep(1)
    
    driver.find_element_by_xpath("//button[@class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only ui-dialog-titlebar-close']//span[@class='ui-button-icon-primary ui-icon ui-icon-closethick']").click()
    logger.info("Close button of drives list window closed successfully")
    time.sleep(1)
    
    driver.find_element_by_xpath("//a[@id='drives-back']").click()
    logger.info("Logout link clicked successfully")
    time.sleep(1)
    
    driver.find_element_by_xpath("//span[@class='nav-underline']").click()
    logger.info("Browser close button clicked successfully")
    time.sleep(1)
    
    logger.info("")
    logger.info("Drives ENTC 209 Script Complete")
    logger.info("================")
    
    driver.close()

except:
    e = sys.exc_info()[0]
    logger.info("Drives ENTC 209 Error" + str(e))
